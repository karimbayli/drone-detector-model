from core import Core

c = Core()
c.set_model(c.get_model())

def processImage():
    image_filename = c.current_path + "/upload_files/img.png"
    image = c.load_image_by_path(image_filename)
    drawing_image = c.get_drawing_image(image)

    processed_image, scale = c.pre_process_image(image)

    boxes, scores, labels = c.predict_with_graph_loaded_model(processed_image, scale)

    detections = c.draw_boxes_in_image(drawing_image, boxes, scores)
    confidence = detections[0]['score']
    print(f'Confidence score for finding a drone is {confidence}%')
    from PIL import Image
    im = Image.fromarray(drawing_image)
    im.save("result.png")
    # c.visualize(drawing_image)
