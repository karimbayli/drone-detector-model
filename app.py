from flask import Flask, jsonify, request, send_file
from werkzeug.utils import secure_filename
import os
import config
import evaluate as e

app = Flask(__name__)

@app.route('/')
def hello():
    return "Hello!"

@app.route('/uploader', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        try:
            data = request.files['file']
            data.save('./upload_files/img.png')
            e.processImage()
        except:
            return jsonify({"status": "no drone is found"})
        return send_file("result.png", mimetype='image/png')
    elif request.method == 'GET':
        return jsonify({"status": "wrong request type"})

if __name__ == '__main__':
    app.debug = True
    app.run()